package br.com.union.utils;

import br.com.union.exceptions.CategoryNotFoundException;
import org.springframework.stereotype.Component;

import static br.com.union.enums.BucketCategorysEnum.UNION_FEED;
import static br.com.union.enums.BucketCategorysEnum.UNION_PROFILE;
import static br.com.union.enums.BucketURLsEnum.UNION_FEED_IMAGES_URL;
import static br.com.union.enums.BucketURLsEnum.UNION_PROFILE_IMAGES_URL;
import static br.com.union.enums.ErrorMessagesEnum.CATEGORY_NOT_FOUND;

@Component
public class UrlFactory {

    public String getUrlBucket(String bucketCategory) {

        if (bucketCategory.equalsIgnoreCase(UNION_PROFILE.getName())) {
            return UNION_PROFILE_IMAGES_URL.getValue();
        }else if(bucketCategory.equalsIgnoreCase(UNION_FEED.getName())) {
            return UNION_FEED_IMAGES_URL.getValue();
        }
        throw new CategoryNotFoundException(CATEGORY_NOT_FOUND.getMessage());
    }


}
