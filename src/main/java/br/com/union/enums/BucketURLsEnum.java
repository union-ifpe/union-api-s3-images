package br.com.union.enums;

public enum BucketURLsEnum {
	UNION_PROFILE_IMAGES_URL("https://union-profile-images.s3.amazonaws.com/"),
	UNION_FEED_IMAGES_URL("https://union-feed-images.s3.amazonaws.com/");

	private String value;

	private BucketURLsEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
