package br.com.union.enums;

public enum BucketNamesEnum {
	UNION_PROFILE_IMAGES_BUCKET("union-profile-images"), 
    UNION_FEED_IMAGES_BUCKET("union-feed-images");

	private String name;

	private BucketNamesEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
