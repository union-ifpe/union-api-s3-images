package br.com.union.enums;

import lombok.Getter;

public enum BucketCategorysEnum {
	UNION_PROFILE("union-profile"),
    UNION_FEED("union-feed");

	@Getter
	private String name;

	public String getName() {
		return name;
	}

	private BucketCategorysEnum(String name) {
		this.name = name;
	}

}
