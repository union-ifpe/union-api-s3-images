package br.com.union.enums;

public enum ErrorMessagesEnum {
	FILE_LARGER_THAN_ALLOWED("Your file size is greater than the maximum allowed"),
	OBJECT_NOT_FOUND("The object was not found in the bucket"),
    CATEGORY_NOT_FOUND("The category was not found");

	private String message;

	private ErrorMessagesEnum(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

}
