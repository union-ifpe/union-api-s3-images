package br.com.union.service;

import br.com.union.model.PhotoResponse;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

public interface UnionImagesService {

    public PhotoResponse putObjectS3(String objectKey, MultipartFile multipartFile, String bucketCategory) throws IOException;

    public PhotoResponse getObjectURL(String objectKey, String bucketCategory);
}
