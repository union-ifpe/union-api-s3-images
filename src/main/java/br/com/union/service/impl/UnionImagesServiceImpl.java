package br.com.union.service.impl;

import static br.com.union.enums.ErrorMessagesEnum.FILE_LARGER_THAN_ALLOWED;
import static br.com.union.enums.ErrorMessagesEnum.OBJECT_NOT_FOUND;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.unit.DataSize;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.services.s3.AmazonS3;

import br.com.union.exceptions.FileLargerThanAllowedException;
import br.com.union.exceptions.ObjectNotFoundException;
import br.com.union.model.PhotoResponse;
import br.com.union.service.UnionImagesService;
import br.com.union.utils.BucketFactory;
import br.com.union.utils.UrlFactory;

@Service
public class UnionImagesServiceImpl implements UnionImagesService {

    private static final long MAXIMUM_FILE_SIZE_BYTES = 5242880;

    @Autowired
    private AmazonS3 amazonS3;

    @Autowired
    private BucketFactory bucketFactory;

    @Autowired
    private UrlFactory urlFactory;

    public PhotoResponse putObjectS3(String objectKey, MultipartFile multipartFile, String bucketCategory) throws IOException {
        checkFileSize(multipartFile);

        File file = this.convertMultiPartToFile(multipartFile);
        amazonS3.putObject(bucketFactory.getBucketName(bucketCategory), objectKey, file);

        return getObjectURL(objectKey, bucketCategory);
    }

    public PhotoResponse getObjectURL(String objectKey, String bucketCategory) {
        checkIfObjectExists(objectKey,bucketCategory);

        String objectUrl = new StringBuilder()
                .append(urlFactory.getUrlBucket(bucketCategory))
                .append(objectKey)
                .toString();

        PhotoResponse photoResponse = new PhotoResponse(objectUrl);
        
        return photoResponse;
    }

    private File convertMultiPartToFile(MultipartFile file) throws IOException {
        File convertedFile = new File(Objects.requireNonNull(file.getOriginalFilename()));
        FileOutputStream fos = new FileOutputStream(convertedFile);
        fos.write(file.getBytes());
        fos.close();
        return convertedFile;
    }

    private void checkFileSize(MultipartFile multipartFile) {
        Long fileSize = Long.valueOf(DataSize.ofBytes(multipartFile.getSize()).toBytes());
        Long fileSizeMaxAllowed = MAXIMUM_FILE_SIZE_BYTES;

        if (fileSize > fileSizeMaxAllowed) {
            throw new FileLargerThanAllowedException(FILE_LARGER_THAN_ALLOWED.getMessage());
        }
    }

    private void checkIfObjectExists(String objectKey, String bucketCategory) {
        if (!amazonS3.doesObjectExist(bucketFactory.getBucketName(bucketCategory), objectKey)) {
            throw new ObjectNotFoundException(OBJECT_NOT_FOUND.getMessage());
        }
    }
}
