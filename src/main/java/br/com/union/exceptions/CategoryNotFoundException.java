package br.com.union.exceptions;

public class CategoryNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -8225157647553684600L;

	public CategoryNotFoundException(String errorMessage) {
        super(errorMessage);
    }

}
