package br.com.union.exceptions;

public class FileLargerThanAllowedException extends RuntimeException {

	private static final long serialVersionUID = 8215550990627917936L;

	public FileLargerThanAllowedException(String errorMessage) {
        super(errorMessage);
    }
}
