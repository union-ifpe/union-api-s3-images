package br.com.union.exceptions;

public class ObjectNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -1562705716909191923L;

	public ObjectNotFoundException(String errorMessage) {
        super(errorMessage);
    }
}
