package br.com.union.controller;

import br.com.union.exceptions.CategoryNotFoundException;
import br.com.union.exceptions.FileLargerThanAllowedException;
import br.com.union.exceptions.ObjectNotFoundException;
import br.com.union.model.ErrorMessage;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ControllerAdvice {

    @ResponseStatus(value = HttpStatus.PAYLOAD_TOO_LARGE)
    @ExceptionHandler(FileLargerThanAllowedException.class)
    public Object handleFileLargerThanAllowedException(FileLargerThanAllowedException e) {    	
    	ErrorMessage errorMessage = new ErrorMessage(e.getMessage(),HttpStatus.PAYLOAD_TOO_LARGE.value());
    	
    	return errorMessage;
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(ObjectNotFoundException.class)
    public Object objectNotFoundException(ObjectNotFoundException e) {
    	ErrorMessage errorMessage = new ErrorMessage(e.getMessage(),HttpStatus.NOT_FOUND.value());
    	
    	return errorMessage;
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(CategoryNotFoundException.class)
    public Object categoryNotFoundException(CategoryNotFoundException e) {
    	
    	ErrorMessage errorMessage = new ErrorMessage(e.getMessage(),HttpStatus.NOT_FOUND.value());
    	
    	return errorMessage;
    }
}
