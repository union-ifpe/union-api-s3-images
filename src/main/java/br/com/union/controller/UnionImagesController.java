package br.com.union.controller;

import br.com.union.model.PhotoResponse;
import br.com.union.service.UnionImagesService;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/images")
public class UnionImagesController {

    @Autowired
    UnionImagesService service;

    @PostMapping
    public ResponseEntity<PhotoResponse> uploadObjectS3(@RequestParam("file") MultipartFile file,
                                                        @RequestParam("imageName") String imageName,
                                                        @RequestHeader("bucketCategory") String bucketCategory) throws IOException {
        return ResponseEntity.status(HttpStatus.CREATED).body(service.putObjectS3(imageName, file, bucketCategory));
    }

    @GetMapping("/{imageName}")
    public ResponseEntity<PhotoResponse> getUrlObjectS3(@PathVariable("imageName") String imageName,
                                                        @RequestHeader("bucketCategory") String bucketCategory) {
        return ResponseEntity.status(HttpStatus.OK).body(service.getObjectURL(imageName, bucketCategory));
    }
}
