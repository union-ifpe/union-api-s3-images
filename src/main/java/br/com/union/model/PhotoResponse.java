package br.com.union.model;

public class PhotoResponse {
	private String url;

	public PhotoResponse(String url) {
		this.url = url;
	}

	public PhotoResponse() {
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
