package br.com.union;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnionApiS3ImagesApplication {

    public static void main(String[] args) {
        SpringApplication.run(UnionApiS3ImagesApplication.class, args);
    }

}
